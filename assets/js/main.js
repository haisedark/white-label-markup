(function() {
  const navSection = document.querySelector('.nav-section');
  const target = document.querySelector('#intersect-helper');

  const observer = new IntersectionObserver(
    entries => {
      for (const entry of entries) {
        const { isIntersecting } = entry;
        navSection.classList.remove('no-transition');
        if (window.matchMedia('(min-width: 768px)').matches) {
          // Desktop
          navSection.classList.toggle('sticked-desktop', !isIntersecting);
        } else {
          // Mobile
          navSection.classList.toggle('sticked-mobile', !isIntersecting);
        }
        setTimeout(() => navSection.classList.add('no-transition'), 400);
      }
    },
    {
      threshold: 1.0,
      rootMargin: '-56px 0px 0px 0px'
    }
  );
  observer.observe(target);

  const resizeListener = () => {
    if (window.matchMedia('(min-width: 768px)').matches) {
      if (navSection.classList.contains('sticked-mobile')) {
        navSection.classList.remove('sticked-mobile');
      }
      return;
    }
    if (navSection.classList.contains('sticked-desktop')) {
      navSection.classList.remove('sticked-desktop');
    }
  };
  window.addEventListener('resize', resizeListener);

  const tooltipToggler = document.querySelector('.info-btn');
  const tooltipRef = tooltipToggler.dataset.tooltipRef;
  const content = document.getElementById(tooltipRef).innerHTML;
  tippy(tooltipToggler, {
    content,
    trigger: 'click',
    animation: 'scale',
    arrow: true,
    interactive: true,
    zIndex: 45,
    onShow: instance => {
      const tooltipToggler = instance.reference;
      tooltipToggler.classList.remove('lnr-info');
      tooltipToggler.classList.add('lnr-cross');
    },
    onHide: instance => {
      const tooltipToggler = instance.reference;
      tooltipToggler.classList.add('lnr-info');
      tooltipToggler.classList.remove('lnr-cross');
    }
  });

  const navExtraShow = document.querySelector('.nav-extra-show');
  const navExtraV1 = document.querySelector('.nav-extra-v1');
  const navExtraV2 = document.querySelector('.nav-extra-v2');
  const navExtraCloseV1 = document.querySelector('.nav-extra-close-v1');
  const navExtraCloseV2 = document.querySelector('.nav-extra-close-v2');
  navExtraShow.addEventListener('click', e => {
    const { ctrlKey } = e;
    if (ctrlKey) {
      navExtraV1.classList.add('shown');
    } else {
      navExtraV2.classList.add('shown');
    }
  });
  navExtraCloseV1.addEventListener('click', () =>
    navExtraV1.classList.remove('shown')
  );
  navExtraCloseV2.addEventListener('click', () =>
    navExtraV2.classList.remove('shown')
  );
})();
