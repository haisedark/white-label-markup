module.exports = {
  prefix: 'wl-',
  theme: {
    container: {
      center: true
    },
    boxShadow: {
      'default': '0 2px 4px 0 rgba(0, 0, 0, 0.1)'
    },
    extend: {
      maxWidth: {
        xxs: '18rem',
        '4xs': '16rem'
      },
      screens: {
        xs: '375px'
      },
      fontFamily: {
        brand: ['Brand', 'sans-serif'],
        'brand-body': ['Roboto', 'sans-serif']
      },
      colors: {
        brand: '#ff6900',
        'brand-dark': '#d96334'
      },
      lineHeight: {
        preloose: '1.75'
      },
      minHeight: {
        x4: '4rem'
      },
      zIndex: {
        '-1': '-1'
      },
      boxShadow: {
        line: '0 1px 0 0 rgba(0, 0, 0, 0.1)',
        'line-top': '0 -1px 0 0 rgba(0, 0, 0, 0.1)'
      }
    }
  },
  variants: {},
  plugins: []
};
